import React, { useState } from 'react';

import './App.css';
import firebase from 'firebase'; 
import { useCollection } from 'react-firebase-hooks/firestore'; 

import useLocalStorage from "./myUseLocalStorage.js";
import { ToastsContainer, ToastsStore } from 'react-toasts';

import ReactTable from 'react-table';
import 'react-table/react-table.css';
import { toDateString, paramToString } from "./utils.js";

const MesReservations = ({ user, bookings, reservations, params }) => {

  const fbMesReserv = useCollection(firebase.firestore().collection('reservations')
    .where("email", "==", user.email));
  var mesReserv = fbMesReserv.loading || fbMesReserv.error ? [] : fbMesReserv.value.docs
    .map(doc => ({
      ...doc.data(), id: doc.id,
      date: new Date(doc.data().date.seconds * 1000)
    }))
    .sort((res1, res2) => res1.date - res2.date);

  //var cancellations = reservAdmin.filter(res => res.bookingType === "off");
  //reservAdmin = reservAdmin.filter(res => res.bookingType !== "off"); 

  function getBookingTitle(id) {
    return bookings ? bookings.find(b => b.id === id).titre : "";
  }

  return (<div class="mesReservations">
    <h4>Mes réservations</h4>
    <div class="firebaseCalendar">

      {(mesReserv && mesReserv.length > 0) ?  
        mesReserv
        .filter(row => {
          return row.date.getTime() >= new Date().setHours(0, 0, 0, 0);})
        .map(res => (<div key={res.id} class={
          "maReservation" + 
          (!res.active ? " cancelled":"")}>
        <span class="type">{getBookingTitle(res.booking)} </span>
        <span class="nbClients"> pour {res.nbClients} personnes le </span>
 <span class="date"> {toDateString(res.date)} </span>
</div>))
:
<div class="aucuneReservations">Aucune Réservation enregistrée à cette addresse courriel</div>}

    </div>
    <ToastsContainer store={ToastsStore} />
  </div>);

};


export default MesReservations;
