import React, { useState } from 'react';
import './App.css';
import firebase from 'firebase';

// Il faut que la page load : <script src="https://js.stripe.com/v3/"></script>

import {
  CardElement,
  CardNumberElement,
  CardExpiryElement,
  CardCVCElement,
  PaymentRequestButtonElement,
  IbanElement,
  IdealBankElement,
  StripeProvider,
  Elements,
  injectStripe,
} from 'react-stripe-elements';

// test card : 5555 5555 5555 4444

const STRIPE_SERVER = "https://impotx.gnitic.com:9000";
//"https://impotx.gnitic.com:9000";
//"http://localhost:9000";
const STRIPE_PUBLIC = "pk_test_PhlpuKfhefRhmwsFHVh4CpoT00Kt6eXYn6";

const handleBlur = () => {
  console.log('[blur]');
};
const handleChange = (change) => {
  console.log('[change]', change);
};
const handleClick = () => {
  console.log('[click]');
};
const handleFocus = () => {
  console.log('[focus]');
};
const handleReady = () => {
  console.log('[ready]');
};

const createOptions = (fontSize, padding) => {
  return {
    style: {
      base: {
        fontSize,
        color: '#424770',
        letterSpacing: '0.025em',
        fontFamily: 'Source Code Pro, monospace',
        '::placeholder': {
          color: '#aab7c4',
        },
        ...(padding ? { padding } : {}),
      },
      invalid: {
        color: '#9e2146',
      },
    },
  };
};

class CardSection extends React.Component {
  render() {
    return (
      <label>
        Card details
        <CardElement style={{ base: { fontSize: '18px' } }} />
      </label>
    );
  }
}

const AddressSection = () => {
  return (
    <label>
      Address details
  {/*<AddressElement style={{ base: { fontSize: '18px' } }} />*/}
    </label>
  );
}


const InjectedCheckoutForm = injectStripe((props) => {
  const handleSubmit = (ev) => {
    ev.preventDefault();

    props.stripe
      .createPaymentMethod('card', { billing_details: { name: 'Jenny Rosen' } })
      .then(({ paymentMethod }) => {
        console.log('Received Stripe PaymentMethod:', paymentMethod);
      });

    props.stripe.handleCardPayment('{PAYMENT_INTENT_CLIENT_SECRET}', props.data);

    props.stripe.createToken({ type: 'card', name: 'Jenny Rosen' });
    props.stripe.createSource({
      type: 'card',
      owner: {
        name: 'Jenny Rosen',
      },
    });
  };

  return (
    <form onSubmit={handleSubmit}>
      <AddressSection />
      <CardSection />
      <button>Confirm order</button>
    </form>
  );
});

function getPaymentRequest(stripe, price) {
  return stripe.paymentRequest({
    country: 'CA',
    currency: 'cad',
    total: {
      label: 'Demo réservation',
      amount: price || 1000,
    },
    requestPayerEmail: true,
  });
}

const InjectedPaymentRequestForm = injectStripe((props) => {
  const [canMakePayment, setcanMakePayment] = useState(false);

  const paymentRequest = getPaymentRequest(props.stripe, props.price);

  paymentRequest.on('token', ({ complete, token, ...data }) => {
    console.log('Received Stripe token: ', token);
    console.log('Received customer information: ', data);
    complete('success');
  });

  paymentRequest.canMakePayment().then((result) => {
    setcanMakePayment(!!result);
  });

  return canMakePayment ? (
    <PaymentRequestButtonElement
      paymentRequest={paymentRequest}
      className="PaymentRequestButton"
      style={{
        paymentRequestButton: {
          theme: 'light',
          height: '64px',
        },
      }}
    />
  ) : null;
});

const MyStoreCheckout = ({ price }) => {
  return (
    <Elements>
      <InjectedPaymentRequestForm price={price} />
    </Elements>
  );
};

const MobileAutoCheckout = ({ price }) => {
  return (
    <Elements>
      <InjectedCheckoutForm price={price} />
    </Elements>
  );
};


const CardForm = injectStripe(({ stripe, onSave, onStartPay, date, user, price, fontSize, reservationData }) => {

  const handleSubmit = (ev) => {
    ev.preventDefault();
    /*"card", {
            country: 'CA',
            currency: 'cad'
        }*/
    if (stripe) {
      stripe
        .createToken()
        .then((payload) => {
          console.log('[token]', payload);
        });
    } else {
      console.log("Stripe.js hasn't loaded yet.");
    }
  };

  console.log("user", user);

  async function submit(ev) {
    console.warn("TODO : Valider avec Firebase qu'on ne book pas en double avant de payer!");

    if (onStartPay) onStartPay(true);

    ev.preventDefault();
    let { token } = await stripe.createToken({ name: user.email });
    if (!token) {
      if (onStartPay) onStartPay(false);
      return;
    }
    console.log('[token]', token);
    var jsonBody = JSON.stringify({
      id: token.id,
      price: price,
      desc: "Réservation " + date + " - " + user.email,
      reservationData
    });
    console.log(jsonBody);
    let response = null;
    response = await fetch(STRIPE_SERVER + "/charge", {
      method: "POST",
      headers: { "Content-Type": "text/plain" },
      body: jsonBody
    }).catch(ex => {
      response = ex;
    });

    if (response && response.ok) {
      if (onSave) onSave();
    }
    else console.error(response);

    console.log("onStartPay false");
    if (onStartPay) onStartPay(false);
  }

  return (
    <form onSubmit={submit}>
      <label>

        <CardElement
          hidePostalCode={true}
          onBlur={handleBlur}
          onChange={handleChange}
          onFocus={handleFocus}
          onReady={handleReady}
          {...createOptions(fontSize)}
        />

      </label>
      <button>Payer et réserver</button>
    </form>
  );
});


const PayForm = (props) => {

  if (props.user) {
    //"pk_test_12345"
    return (
      <StripeProvider apiKey={STRIPE_PUBLIC}>
        <div class="paymentSection">
          {/*<MobileAutoCheckout {...props} />
        <MyStoreCheckout {...props} />
  */}
          <Elements>
            <CardForm fontSize="14px" {...props} />
          </Elements>
        </div>
      </StripeProvider>);
  } else {
    return <div class="paymentSection needLogin">Connectez vous pour payer</div>;
  }
};

export default PayForm;
