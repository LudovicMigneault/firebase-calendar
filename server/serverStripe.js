const app = require("express")();
const spdy = require('spdy');
const fs = require("fs");
const nodemailer = require('nodemailer');
//sk_test_mls6AT9BYWpE3tEQVXqQ38bI00TCwwHJuO
const stripe = require("stripe")("sk_test_mls6AT9BYWpE3tEQVXqQ38bI00TCwwHJuO");

//const firebase = require("firebase"); 
var admin = require("firebase-admin");
const { map, reduce, indexBy } = require("rambda");

var serviceAccount = require("./nath-e8520-firebase-adminsdk-m13ec-9fc76e8ce1.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://nath-e8520.firebaseio.com"
});

app.use(require("body-parser").text());

const SERVER_KEY_PATH = "/etc/letsencrypt/live/impotx.gnitic.com/privkey.pem";
const SERVER_CERT_PATH = "/etc/letsencrypt/live/impotx.gnitic.com/fullchain.pem";

const options = {
  cert: fs.readFileSync(SERVER_CERT_PATH, 'utf8'), //fs.readFileSync("key.pem"),//
  key: fs.readFileSync(SERVER_KEY_PATH, 'utf8'), // fs.readFileSync("cert.pem"),//
};

var params = {
  emailsAdmin: ["ludovic@gnitic.com"],
  prixDiner: 40,
  prixSouper: 80,
  minClients: 10,
  maxClients: 30
};


admin.firestore().collection('adminParameters').orderBy("dateChangement", "desc").limit(1).onSnapshot(snapshot => {
  snapshot.docChanges().forEach(function (change) {
    if (change.type === "added") {
      console.log(change.doc.data());
      for (var paramName in change.doc.data()) {
        params[paramName] = change.doc.data()[paramName];
      }
    }
  });
});

var bookings = {};
admin.firestore().collection('bookingTemplate').onSnapshot(snapshot => {
  snapshot.docChanges().forEach(function (change) {
    bookings[change.doc.id] = change.doc.data();
    console.log("booking" + JSON.stringify(bookings));
  });
});


var transporter = nodemailer.createTransport({
  service: 'Gmail',
  auth: {
    user: 'ludovic@gnitic.com',
    pass: 'Jensen011949'
  }
});

var mailOptions = {
  from: 'ludovic@gnitic.com',
  to: 'ludovic@gmail.com',
  subject: 'Réservation avec Chef Nath',
  text: 'Votre réservation as bien été enregistrée!'
};


function sendMail(to, titre, text) {
  try {
    transporter.sendMail({ ...mailOptions, to, text, subject: titre },
      function (error, info) {
        if (error) {
          console.error("Couldn't send mail to " + to);
        } else {
          console.log('Email sent: ' + info.response);
        }
      });
  } catch (ex) {
    console.error("Couldn't send mail to " + to);
  }
}

function calculatePrice(nbPersonnes, typeRepas) {
  if (typeRepas === "souper") {
    return _prixSouper * 2;
  } else if (typeRepas === "diner") {
    return _prixDiner * 2;
  }
  else {
    console.error("Type repas n'est pas valide (souper|diner)" + typeRepas);
  }
}

function dateToString(date) {
  if (date) {
    return new Date(date.seconds * 1000).toLocaleString();
  } else {
    return "";
  }
}

function createMailText(data) {
  return "Réservation pour " + data.nbClients + " clients au nom de "
    + data.prenom + " " + data.nom + " (" + data.email + ")" +
    " le " + dateToString(data.date);
};

app.post("/charge", async (req, res) => {
  try {
    console.log("Charge - req : ");
    console.log(req.body);

    var body = JSON.parse(req.body);
    var paid = body.price; // || calculatePrice()

    var previous = await admin.firestore().collection("reservations")
      .where("date", "==", body.reservationData.date)
      .where("booking", "==", body.reservationData.booking)
      //.limit(1)
      .get();

    //console.log(previous);
    //console.log(previous.empty);
    //console.log(previous.size);
    var availabilities = getAvailablities(
      body.reservationData.date,
      previous,
      bookings[body.reservationData.booking]);
    console.log("Avail.");
    console.log(availabilities);

    if (availabilities > 0) {
      //previous.empty) {
      let { status } = await stripe.charges.create({
        amount: paid,//2000,
        currency: "cad",
        description: body.desc,
        source: body.id
      });
      console.log("Charge - status : ", status);

      await saveFirebase(paid, body.reservationData);

      sendMail(body.reservationData.email,
        "Réservation Chef Nath - " + dateToString(body.reservationData.date),
        createMailText(body.reservationData));

      (params.emailsAdmin || [])
        .forEach(mail => sendMail(mail, "Réservation Chef Nath - " +
          dateToString(body.reservationData.date),
          createMailText(body.reservationData)));

      res.json({ status });
    } else {
      console.warn("Quelqu'un as essayé de réserver sur une date existante.");

      sendMail(body.reservationData.email, "Erreur, double réservation",
        "Erreur de réservation, " + body.reservationData.email +
        " as tenté de réserver pour le " + dateToString(body.reservationData.date));
      res.status(500).end();
    }
  } catch (err) {
    console.error(err);

    sendMail(body.reservationData.email, "Erreur lors de la réservation",
      "Erreur de réservation, " + body.reservationData.email +
      " as tenté de réserver pour le " + dateToString(body.reservationData.date));

    res.status(500).end();
  }
});


async function saveFirebase(paidAmount, { booking, date, user, nbClients, ...adminReservation }) {
  //nbClients, details, prenom, nom, email, tel}) {

  const db = admin.firestore();

  var reservationPub = {
    booking,
    date,
    user,
    nbClients
  };

  var docRef = await db.collection("reservationsPublic").add(reservationPub)
    .catch(err => {
      console.error("could not save Public reservation");
      console.error(err);
      throw (err);
    });

  console.log("Document written with ID: ", docRef.id);

  var docFullRef = await db.collection("reservations").add({
    ...reservationPub, ...adminReservation,
    publicReservation: docRef.id, active: true
    /*bookingType: "meal",
      paidAmount,
      nbClients,
      details: desc,
      prenom,
      nom,
      email,
      tel*/
  }).catch(err => {
    console.error("could not save full reservation");
    console.error(err);
    throw (err);
  });

  console.log("Full Document written with ID: ", docFullRef.id);
}

//app.listen(9000, () => console.log("Listening on port 9000 - Stripe server"));

console.log("Start Spdy on port 9000 - Stripe server");
spdy
  .createServer(options, app)
  .listen(9000);


function dateToId(day) {
  var toDate = new Date(day.seconds * 1000);
  return toDate.getFullYear() + "-" + toDate.getMonth() + "-" + toDate.getDate();
}

function getAvailablities(date, reservations, booking) {
  /*console.log("getAvailabilities"); 
  console.log("_docs");
  console.log(reservations._docs().map(res=>res.data())); */

  var bookedGrouped = reduce((t, res) => {
    if (res.adminBlock) return t + 1000;
    if (res.adminBlock === false) return t;
    return t +
      (isNaN(res.nbClients) ?
        1000 : res.nbClients);
  }, 0, reservations._docs().map(res => res.data()));

  //bookedGrouped = bookedGrouped[dateToId(date)];
  //console.log(bookedGrouped);

  var ret = 0;

  if (booking.canBookManyGroups) {
    ret = params.maxClients - bookedGrouped;
  } else {
    ret = bookedGrouped > 0 ? 0 : params.maxClients;
  }

  if (ret < params.minClients) {
    ret = 0;
  }

  return ret;
}